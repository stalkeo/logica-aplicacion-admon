package g8107.Datos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the productos database table.
 * 
 */
@Entity

@Table(name="productos")
@NamedQueries ({
	@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p"),
	@NamedQuery(name = "Producto.findbyId", query = "SELECT p FROM Producto p WHERE p.id = :id"),
	@NamedQuery (name = "Producto.findbyPropietario", query = "SELECT p FROM Producto p WHERE p.usuario.eMail = :propietario"),
	@NamedQuery (name = "Producto.findByUltimoId", query = "SELECT p FROM Producto p ORDER BY p.id DESC"),	
	@NamedQuery (name = "Producto.findbyCategoria", query = "SELECT p FROM Producto p WHERE p.categoria = :categoria"),
	@NamedQuery(name = "Producto.advSearch", query = "SELECT p FROM Producto p WHERE (:categoria = 'Todo' OR p.categoria = :categoria) "
																				+ "AND (:provincia = '' OR p.usuario.ciudad = :provincia) "
																				+ "AND (:vendedor = '' OR p.usuario.nombre LIKE :vendedor OR p.usuario.apellido1 LIKE :vendedor OR p.usuario.apellido2 LIKE :vendedor OR p.usuario.eMail LIKE :vendedor) "
																				+ "AND (:titulo = '' OR p.titulo LIKE :titulo) "
																				+ "AND (:descripcion = '' OR p.descripcion LIKE :descripcion)")

})
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String categoria;

	private int estado;

	@Column(name="num_img")
	private int numImg;

	private float precio;

	private String titulo;
	
	private String descripcion;

	//bi-directional many-to-one association to Imagene
	@OneToMany(mappedBy="producto")
	private List<Imagene> imagenes;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="propietario")
	private Usuario usuario;

	


	public Producto(int id, String categoria, int estado, int numImg, float precio, String titulo, String descripcion,
			List<Imagene> imagenes, Usuario usuario) {
		super();
		this.id = id;
		this.categoria = categoria;
		this.estado = estado;
		this.numImg = numImg;
		this.precio = precio;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.imagenes = imagenes;
		this.usuario = usuario;
	}

	public Producto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getNumImg() {
		return this.numImg;
	}

	public void setNumImg(int numImg) {
		this.numImg = numImg;
	}

	public float getPrecio() {
		return this.precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Imagene> getImagenes() {
		return this.imagenes;
	}

	public void setImagenes(List<Imagene> imagenes) {
		this.imagenes = imagenes;
	}

	public Imagene addImagene(Imagene imagene) {
		getImagenes().add(imagene);
		imagene.setProducto(this);

		return imagene;
	}

	public Imagene removeImagene(Imagene imagene) {
		getImagenes().remove(imagene);
		imagene.setProducto(null);

		return imagene;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}