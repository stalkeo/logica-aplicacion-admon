package g8107.Datos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;

import java.util.List;

public class UsuarioManager {

	private EntityManagerFactory emf;

	public UsuarioManager(){
	}

	/*Se deve pasar por parametro el ENtityManagerFactory correspondiente*/
	public UsuarioManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	/*Se permite el cambio de EntityManagerFactory*/
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	/*Devuelve un EntityManager, si el EntityManagerFactory no esta creado devolverá null*/
	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	/*Inserta un usuario en la base de datos completo*/
	private boolean createUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(Usuario);
			em.getTransaction().commit();
		} 
		catch (Exception ex) {
			try {
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
			} 
			catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} 
		finally {
			em.close();
		}
		return true;
	}
	
	/*Inserta un usuario en la base de datos solo con las columnas de la tabla Usuarios*/
	private boolean createUsuarioMin(UsuarioMin usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(usuario);
			em.getTransaction().commit();
		} 
		catch (Exception ex) {
			try {
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
			} 
			catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} 
		finally {
			em.close();
		}
		return true;
	}
	
	/*Borrar un usuario en la base de datos completo*/
	private boolean deleteUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario = em.merge(Usuario);
			em.remove(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	/*Borrar un usuario en la base de datos solo con las columnas de la tabla Usuarios*/
	private boolean deleteUsuarioMin(UsuarioMin Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario= em.merge(Usuario);
			em.remove(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	/*Actualizar un usuario en la base de datos completo*/
	private boolean updateUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario = em.merge(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	/*actualizar un usuario en la base de datos solo con las columnas de la tabla Usuarios*/
	private boolean updateUsuarioMin(UsuarioMin Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario = em.merge(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}

	/*Busca un usuario completo por su email*/
	public Usuario findUsuarioByEmail(String eMail) {
		Usuario Usuario = null;
		EntityManager em = getEntityManager();
		
		try {
			Usuario = (Usuario) em.find(Usuario.class, eMail);
		} 
		finally {
			em.close();
		}
		return Usuario;
	}
	
	/*Busca un usuario con los atributos minimos por su email*/
	public UsuarioMin findUsuarioMinByEmail(String eMail) {
		UsuarioMin Usuario = null;
		EntityManager em = getEntityManager();
		
		try {
			Usuario = (UsuarioMin) em.find(UsuarioMin.class, eMail);
		} 
		finally {
			em.close();
		}
		return Usuario;
	}

	/*Función de validación de los administradores*/
	public boolean validateAdmin(String eMail, String pass) {
		UsuarioMin Usuario = null;
		Usuario = findUsuarioMinByEmail(eMail);
		if(Usuario !=null){
			if(Usuario.getAdministrador()){
				if(Usuario.getEMail().equals(eMail) && Usuario.getPassword().equals(pass))
					return true;
			}
		}
		return false;
	}
	
	/*Función de creación de nuevos usuarios por administradores*/
	public int insertarUserbyAdmin(String e_mail, String pass, String nombre, String ap1, String ap2, String cod_postal, String provincia, boolean administrador)  {
		UsuarioMin Usuario = findUsuarioMinByEmail(e_mail);
		if(Usuario == null){
			Usuario = new UsuarioMin(e_mail, pass, nombre, ap1,  ap2,  Integer.parseInt(cod_postal), provincia,administrador);
			try {
				if(createUsuarioMin(Usuario)) return 1;
			} 
			catch (Exception e) {
				/*La excepción no pasa al servlet*/
				e.printStackTrace();
			}
			return 3;
		}
		return 2;
	}
	
	/*Función que modifica los datos de los usuarios*/
	public int ModificarUser(String e_mail, String pass, String nombre, String ap1, String ap2, String cod_postal, String provincia, String pass_old)  {
		UsuarioMin Usuario = findUsuarioMinByEmail(e_mail);
		if(!pass_old.isEmpty()){
			if(!Usuario.getPassword().equals(pass_old))
				return 2;
			Usuario.setPassword(pass);
		}
		else{
			if(!nombre.isEmpty())
				Usuario.setNombre(nombre);
			if(!ap1.isEmpty())
				Usuario.setApellido1(ap1);
			if(!ap2.isEmpty())
				Usuario.setApellido2(ap2);
			if(!cod_postal.isEmpty())
				Usuario.setCodPostal(Integer.parseInt(cod_postal));
			if(!provincia.isEmpty())
				Usuario.setCiudad(provincia);
		}

		try {
			if(updateUsuarioMin(Usuario)) return 1;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return 3;
	}
	
	/*Modificacion de usuarios por un administrador*/
	public boolean ModificarUserbyAdmin(String e_mail, String pass, String nombre, String ap1, String ap2, String cod_postal, String provincia, boolean administrador)  {
		UsuarioMin Usuario = findUsuarioMinByEmail(e_mail);
		if(!pass.isEmpty())
			Usuario.setPassword(pass);
		if(!nombre.isEmpty())
			Usuario.setNombre(nombre);
		if(!ap1.isEmpty())
			Usuario.setApellido1(ap1);
		if(!ap2.isEmpty())
			Usuario.setApellido2(ap2);
		if(!cod_postal.isEmpty())
			Usuario.setCodPostal(Integer.parseInt(cod_postal));
		if(!provincia.isEmpty())
			Usuario.setCiudad(provincia);
		if(administrador != Usuario.getAdministrador())
			Usuario.setAdministrador(administrador);
		try {
			if(updateUsuarioMin(Usuario)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	
	
	/*Función de dar de baja usuarios, se usa el usuario con todos sus elementos para borrar todo el contenido*/
	public int BajaUser(String e_mail, String contrasena)  {
		Usuario Usuario = findUsuarioByEmail(e_mail);
		if(!contrasena.isEmpty()){
			if(Usuario.getPassword().equals(contrasena)){
				try {
					if(deleteUsuario(Usuario)) return 1;
				} 
				catch (Exception e) {
					/*La excepción no pasa al servlet*/
					e.printStackTrace();
				}
			}
			else{
				return 2;
			}
		}
		return 3;
	}
	
	/*Función de dar de baja usuarios realizada por administradores,se usa el usuario con todos sus elementos para borrar todo el contenido*/
	public boolean BajaUserbyAdmin(String e_mail)  {
		Usuario Usuario = findUsuarioByEmail(e_mail);
		try {
			if(deleteUsuario(Usuario)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	
	/*Obtiene todos los usuarios y sus objetos*/
	public List<Usuario> obtenerAllUsers(){
		List<Usuario> results = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Usuario> query = em.createNamedQuery("Usuario.findAll", Usuario.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Obtiene los minimos atributos de todos los usuarios*/
	public List<UsuarioMin> obtenerAllUsersMin(){
		List<UsuarioMin> results = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<UsuarioMin> query = em.createNamedQuery("UsuarioMin.findAll", UsuarioMin.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Devuele todos los usuarios desde la tupla init y se cogen tuplas hasta numElements*/
	public List<UsuarioMin> obtenerUsersMin_number(int init, int numElements){
		List<UsuarioMin> results = null;
		EntityManager em = getEntityManager();
		try{
			TypedQuery<UsuarioMin> query = em.createNamedQuery("UsuarioMin.findAll", UsuarioMin.class);
			query.setFirstResult(init);
			query.setMaxResults(numElements);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Devuelve todos los usuarios que comiencen por una letra*/
	public List<UsuarioMin> obtenerAllUsersMin_word(String word){
		List<UsuarioMin> results = null;
		EntityManager em = getEntityManager();
		try{
			TypedQuery<UsuarioMin> query = em.createQuery("SELECT u FROM UsuarioMin u WHERE (u.eMail LIKE '"+word+"%')", UsuarioMin.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Devuelve los usuarios que comiencen por una letra desde la tupla init y se devuelve tuplas hasta numElements*/
	public List<UsuarioMin> obtenerUsersMin_word_number(int init, int numElements,String word){
		List<UsuarioMin> results = null;
		EntityManager em = getEntityManager();
		try{
			TypedQuery<UsuarioMin> query = em.createQuery("SELECT u FROM UsuarioMin u WHERE (u.eMail LIKE '"+word+"%')", UsuarioMin.class);
			query.setFirstResult(init);
			query.setMaxResults(numElements);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	

}

