package g8107.Datos;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Usuarios database table.
 * 
 */
@Entity
@Table(name="usuarios")
@NamedQueries({
	@NamedQuery(name="UsuarioMin.findAll", query="SELECT u FROM UsuarioMin u"),
	@NamedQuery(name="UsuarioMin.findAll.word", query="SELECT u FROM UsuarioMin u WHERE (u.eMail LIKE :word )")
})
public class UsuarioMin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="e_mail")
	private String eMail;

	private boolean administrador;

	private String apellido1;

	private String apellido2;

	private String ciudad;

	@Column(name="cod_postal")
	private int codPostal;

	private String nombre;

	private String password;

	public UsuarioMin() {
	}
	
	public UsuarioMin(String eMail, String password, String nombre, String apellido1, String apellido2, int codPostal, String ciudad,
			 boolean administrador) {
		super();
		this.eMail = eMail;
		this.administrador = administrador;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.ciudad = ciudad;
		this.codPostal = codPostal;
		this.nombre = nombre;
		this.password = password;
	}


	public String getEMail() {
		return this.eMail;
	}

	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	public boolean getAdministrador() {
		return this.administrador;
	}

	public void setAdministrador(boolean administrador) {
		this.administrador = administrador;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getCodPostal() {
		return this.codPostal;
	}

	public void setCodPostal(int codPostal) {
		this.codPostal = codPostal;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}