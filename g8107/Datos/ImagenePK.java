package g8107.Datos;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the imagenes database table.
 * 
 */
@Embeddable
public class ImagenePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_prod", insertable=false, updatable=false)
	private int idProd;

	@Column(name="num_img")
	private int numImg;

	public ImagenePK() {
	}
	public int getIdProd() {
		return this.idProd;
	}
	public void setIdProd(int idProd) {
		this.idProd = idProd;
	}
	public int getNumImg() {
		return this.numImg;
	}
	public void setNumImg(int numImg) {
		this.numImg = numImg;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ImagenePK)) {
			return false;
		}
		ImagenePK castOther = (ImagenePK)other;
		return 
			(this.idProd == castOther.idProd)
			&& (this.numImg == castOther.numImg);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idProd;
		hash = hash * prime + this.numImg;
		
		return hash;
	}
}