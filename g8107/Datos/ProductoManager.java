package g8107.Datos;

import java.math.BigDecimal;
import java.util.List;

import javax.jdo.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

public class ProductoManager {

	private EntityManagerFactory emf;
	
	public ProductoManager() {

	}
	/*Para crear el ProductoManager, debemos pasarle un EntityManagerFactory*/
	public ProductoManager(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	/*Es posible modificar el EntityManagerFactory introducido*/
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	/*Nos devuelve el EntitymanagerFactiry asociado*/
	public EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}
	
	/*Crea un producto en la DB*/
	public boolean createProducto(Producto producto){
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(producto);
			em.getTransaction().commit();
		}
		
		catch (Exception ex) {
			try {
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
			} 
			catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} 
			
		finally {
			em.close();
		}
		return true;
	}
	
	/*Elimina un usuario de la DB*/
	public boolean deleteProducto(Producto producto) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			producto = em.merge(producto);
			em.remove(producto);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	public boolean updateProducto(Producto producto) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			producto = em.merge(producto);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	public boolean insertarProducto(int id, String categoria, int estado, int numImg, float precio, String titulo, String descripcion, List<Imagene> imagenes, Usuario propietario){
		
		Producto producto = new Producto (id, categoria, estado, numImg, precio, titulo, descripcion, imagenes, propietario);
		
		try{
			if (createProducto(producto)) return true;
		}
		catch(Exception e){
			e.printStackTrace();
		}
	
		return false;
	}
	
	/*Funcion que modifica los datos del producto*/
	public boolean modificarProducto(int id, String categoria, int estado, int numImg, float precio, String titulo, String descripcion, List<Imagene> imagenes, Usuario propietario)  {
		
		Producto producto = findProductoByID(id);
	
		if(!categoria.isEmpty())
			producto.setCategoria(categoria);
		if(estado >= 0 && estado <= 2)
			producto.setEstado(estado);
		/*No implementado numImg --> Disparador*/
		if(precio >= 0)
			producto.setPrecio(precio);
		if(!titulo.isEmpty())
			producto.setTitulo(titulo);
		if(!descripcion.isEmpty())
			producto.setDescripcion(descripcion);
		
		
		try {
			if(updateProducto(producto)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	/*Busca un producto por su ID*/
	public Producto findProductoByID(int id) {
		
		Producto producto = null;
		EntityManager em = getEntityManager();
		
		try {
			
			producto = (Producto) em.createNamedQuery("Producto.findbyId").setHint(QueryHints.REFRESH, HintValues.TRUE).setParameter("id", id).getSingleResult();
			
			
			/*producto = (Producto) em.find(Producto.class, id);*/
		} 
		finally {
			em.close();
		}
		
		return producto;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> findProductoByPropietario(String email){
		
		List <Producto> results = null;
		EntityManager em = getEntityManager();
		
		try {
			results = (List <Producto>) em.createNamedQuery("Producto.findbyPropietario").setParameter("propietario", email).getResultList();
		}
		finally{
			em.close();
		}
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List <Producto> findProductoByCategoria (String categoria){
		
		List <Producto> results = null;
		EntityManager em = getEntityManager();
		
		try{
			
			results = (List <Producto>) em.createNamedQuery("Producto.findbyCategoria").setParameter("categoria", categoria).getResultList();
		}
		finally{
			em.close();
		}
		
		return results;
	
	}
	
	@SuppressWarnings("unchecked")
	public List <Producto> findProductoByIdUltimo (){
		
		List <Producto> results = null;
		EntityManager em = getEntityManager();
		
		try {
		
			results = (List <Producto>) em.createNamedQuery("Producto.findByUltimoId").setFirstResult(0).setMaxResults(1).getResultList();
		}
		finally {
			em.close();
		}
		
		return results;
	}
	
	
	public List <Producto> advSearch (String categoria, String provincia, String vendedor, String titulo, String descripcion){
		
		List <Producto> results = null;
		EntityManager em = getEntityManager();
		
		try {
			results = em.createNamedQuery("Producto.advSearch", Producto.class).setParameter("categoria", categoria).setParameter("provincia", provincia).setParameter("vendedor", "%" + vendedor + "%").setParameter("titulo", "%" + titulo + "%").setParameter("descripcion", "%" + descripcion + "%" ).getResultList();
		}
		finally {
			em.close();
		}
				
		return results;
	}

	/*Obtiene todos los productos */
	public List<Producto> obtenerAllProducts(){
		List<Producto> results = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Producto> query = em.createNamedQuery("Producto.findAll", Producto.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	
	/*Obtiene todos los productos con coincidencia en determinadas palabras en la descripción y titulo*/
	public List<Producto> obtenerAllProducts_words(String words){
		List<Producto> results = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Producto> query = em.createQuery("SELECT u FROM Producto u WHERE u.titulo LIKE '%"+words+"%' OR u.descripcion LIKE '%"+words+"%'", Producto.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Obtiene todos los productos para una determinada categoria y coincidencias*/
	public List<Producto> obtenerAllProducts_wordsAndCategory(String words, String category){
		List<Producto> results = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Producto> query = em.createQuery("SELECT u FROM Producto u WHERE u.categoria = '"+ category + "' AND (u.titulo LIKE '%"+words+"%' OR u.descripcion LIKE '%"+words+"%')", Producto.class);
			results = query.getResultList();
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return results;
	}
	
	/*Elimina un producto por un administrador*/
	public boolean BajaProductbyAdmin(String ID){
		Producto Producto = null;
		Producto = findProductoByID(Integer.parseInt(ID));
		try {
			deleteProducto(Producto);
			return true;
		}
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	

}
