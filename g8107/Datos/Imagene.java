package g8107.Datos;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the imagenes database table.
 * 
 */
@Entity
@Table(name="imagenes")

@NamedQueries ({
@NamedQuery(name="Imagene.findAll", query="SELECT i FROM Imagene i"),
@NamedQuery (name = "Imagene.findbyProducto", query = "SELECT i FROM Imagene i WHERE i.producto.id = :id"),
@NamedQuery (name = "Imagene.findbyIdImagen", query = "SELECT i FROM Imagene i WHERE i.id.idProd = :id_prod AND i.id.numImg = :id_num_img" )

})
public class Imagene implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ImagenePK id;

	@Lob
	private byte[] imagen;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_prod")
	private Producto producto;

	
	public Imagene(ImagenePK id, byte[] imagen, Producto producto) {
		super();
		this.id = id;
		this.imagen = imagen;
		this.producto = producto;
	}

	public Imagene() {
	}

	public ImagenePK getId() {
		return this.id;
	}

	public void setId(ImagenePK id) {
		this.id = id;
	}

	public byte[] getImagen() {
		return this.imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}