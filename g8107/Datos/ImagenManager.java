package g8107.Datos;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

public class ImagenManager {

	private EntityManagerFactory emf;
	
	/*Para crear el ImageManager, debemos pasarle un EntityManagerFactory*/
	public ImagenManager(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	
	/*Es posible modificar el EntityManagerFactory introducido*/
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	
	/*Devuelve un EntityManager, si el EntityManagerFactory no esta creado devolverá null*/
	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}
	
	
	/*Inserta una imagen en la base de datos*/
	private boolean createImagen(Imagene imagen) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(imagen);
			em.getEntityManagerFactory().getCache().evictAll(); /*Necesitamos resetear la cache. Si no, cogemos imagenes erroneas*/
			em.getTransaction().commit();
			
		} 
		catch (Exception ex) {
			try {
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
			} 
			catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} 
		finally {
			em.close();
		}
		return true;
	}
	
	private boolean deleteImagen(Imagene imagen) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			imagen= em.merge(imagen);
			em.remove(imagen);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	
	private boolean updateImagene(Imagene image) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			image = em.merge(image);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}
	
	
	/*Para insertar una imagen, necestamos su PK (id producto y num_img), la imagen, y el Producto al que referencia*/
	public boolean insertarImagen(ImagenePK id, byte[] imagenP , Producto producto)  {
		Imagene imagen = new Imagene (id, imagenP, producto);
		
		try {
			if(createImagen(imagen)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	
	/*Funcion que modifica los datos del producto*/
	public boolean modificarImagen(ImagenePK id, byte[] imagenP , Producto producto)  {
		
		Imagene imagen = obtenerImagenByIdImagen(id);
		
		imagen.setId(id);
		imagen.setImagen(imagenP);
		imagen.setProducto(producto);
		
		try {
			if (updateImagene(imagen)) return true;
		}
		
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		System.out.println("hola7");
		return false;
	
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List <Imagene> obtenerImagenesByProducto(int id){
		
		List <Imagene> results = null;
		EntityManager em = getEntityManager();
		
		try {
			
			results = (List <Imagene>) em.createNamedQuery("Imagene.findbyProducto").setHint(QueryHints.REFRESH, HintValues.TRUE).setParameter("id", id).getResultList();
		}
		
		finally{
			em.close();
		}
		
		return results;
	}
	
	public Imagene obtenerImagenByIdImagen (ImagenePK idO){
		
		Imagene result = null;
		EntityManager em = getEntityManager();
		
		try {
			
			result = em.createNamedQuery("Imagene.findbyIdImagen", Imagene.class).setParameter("id_prod", idO.getIdProd()).setParameter("id_num_img", idO.getNumImg()).getSingleResult();
		}
		
		finally {
			em.close();
		}
		
		return result;
	}
	
}
