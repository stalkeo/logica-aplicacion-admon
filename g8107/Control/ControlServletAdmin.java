package g8107.Control;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import g8107.Datos.ImagenManager;
import g8107.Datos.Imagene;
import g8107.Datos.ImagenePK;
import g8107.Datos.Producto;
import g8107.Datos.ProductoManager;
import g8107.Datos.Usuario;
import g8107.Datos.UsuarioManager;
import g8107.Datos.UsuarioMin;
import g8107.Chat.JMSManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;

/**
 * Servlet implementation class ControlServlet
 */
@WebServlet(name="ControlServletAdmin",urlPatterns={"/ControlServletAdmin"})
public class ControlServletAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ServletContext miServletContex = null;
	
	private String path = "/home/panta/Documentos/servers/glassfish4/glassfish/domains/domain1/eclipseApps/g8107-EAR/g8107-Admon_war/temp/";
	
    public ControlServletAdmin() {
        super();
    }
    
    public void init() {
    	//Se coge el contexto de servlet
		miServletContex = getServletContext();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Se declara el requestdispatcher.
		RequestDispatcher reqDis;
		
		/*Se declara el ENtityManagerFactory que se va utilizar para todas las peticiones a la base de datos*/
		EntityManagerFactory emf= Persistence.createEntityManagerFactory("g8107-Admon");
		
		/*Declaración de el objeto userManager que realizará los cambios en la tabla de usuarios*/
		UsuarioManager userManager = new UsuarioManager(emf);
		
		//Variable auxiliar para obtener el aprametro que indica que tipo de peticion se realiza.
		String aux;
		aux = request.getParameter("n_peticion");
		
		/*Solo si se a reenviado una petición el propio servlet este atributo tendra valor*/
		if(request.getAttribute("n_peticion_A") !=null)
			aux = (String) request.getAttribute("n_peticion_A");
		
		/*Se convierte a entero para que no sucedan errores en el switch*/
		int peticion = Integer.parseInt(aux);
		
		/*Declaracion del objeto productManager que realizara los cambios en la tabla productos*/
		ProductoManager productManager = new ProductoManager(emf);
	
		/*Declaracion del objeto productManager que realizara los cambios en la tabla productos*/
		ImagenManager imagenmanager = new ImagenManager(emf);
		
		switch(peticion){
		/*Inicio de sesion*/
		case 1:	
			
			//Variables necesarias
			boolean validacion;
			
			//Establecemos la conexion a la base de datos y se realiza la consulta.
			validacion = userManager.validateAdmin(request.getParameter("e_mailL"),request.getParameter("contrasenaL"));
			
			//Si al validacion es correcta
			if(validacion){
				
				/*Se indica en la terminal que hay un nuevo inicio de sesion*/
				System.out.println("Nuevo inicio de sesion Administrador Email: " + request.getParameter("e_mailL"));
				
				/*Se genera la session*/
				HttpSession session = request.getSession(true);
				
				/*A la sesion se le asocia el e_mail del administrador*/
				session.setAttribute("e_mail",request.getParameter("e_mailL"));
				
				/*Se indica en la sesion en que ventana de administración se encuentra el administrador*/
				session.setAttribute("Ventana","0");
				
				/*Se pone el valor de error de la request a -1 para indicar que no hay errores*/
				request.setAttribute("error", -1);
				
				/*Se hace la consulta de los propios datos del administrador*/
				UsuarioMin usuario = userManager.findUsuarioMinByEmail((String) session.getAttribute("e_mail"));

				/*Se mandan los datos en la request*/
				request.setAttribute("user_data", usuario);
				
				/*Se envia al administrador a la pagina de administracion*/
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
			}
			
			else{
				
				/*Si sucede un error se indica en la terminal*/
				System.out.println("Fallo en el inicio de sesion de un administrador");
				
				/*En el caso del index el error 0 significa error de inicio de sesion*/
				request.setAttribute("error", 0);
				
				/*Se reenvia al index*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
			}
			
			break;
			
		/*Modificación de datos propios del administrador*/
		case 2:
			
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			
			/*Si hay sesion*/
			else{
				
				/*Se hace la consulta de los propios datos del administrador*/
				UsuarioMin usuario = userManager.findUsuarioMinByEmail((String)request.getSession(false).getAttribute("e_mail"));

				/*Se mandan los datos en la request*/
				request.setAttribute("user_data", usuario);
				
				/*Se indica en la sesión en que ventana de administración se encuentra el administrador*/
				request.getSession(false).setAttribute("Ventana","0");
				
				/*Se pone el valor de error a -1 para indicar que no hay errores*/
				request.setAttribute("error", -1);
				
				
				/*Se envia al administrador a la pagina de administracion*/
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Gestión de usuarios*/
		case 3:	
			
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			
			/*Si hay sesion*/
			else{

				/*Se indica en la sesión en que ventana de administración se encuentra el administrador*/
				request.getSession(false).setAttribute("Ventana","1");
				
				/*Varibale auxiliar para conocer desde que numero de tupla se van a devolver los valores*/
				int init_user =0;
				
				/*Si no se recibe una letra se empiezan a mostrar todos los usuarios*/
				if(request.getParameter("letra_busqueda") == null && request.getAttribute("letra_busqueda_A") == null){

					/*Siempre viene un init_user*/
					/*Procedente del JSP*/
					if(request.getParameter("init_user") != null)
						init_user = Integer.parseInt(request.getParameter("init_user"));
					/*Procedente del servlet*/
					if(request.getAttribute("init_user_A") != null)
						init_user = (int) request.getAttribute("init_user_A");
					
					/*Se piden los usuarios que se se van a administrar*/
					List<UsuarioMin> allUser = userManager.obtenerUsersMin_number(init_user,30);
					
					/*Se actualiza el atributo de la request*/
					init_user =  init_user + 30;
					request.setAttribute("init_user_A",init_user);
					
					/*Se devuelven en un atributo de la request*/
					request.setAttribute("allUser", allUser);
					
				}
				
				/*Si se ha recibido una letra*/
				else if (request.getParameter("letra_busqueda") != null || request.getAttribute("letra_busqueda_A")!=null){
					
					/*Siempre viene un init_user*/
					/*Procedente del JSP*/
					if(request.getParameter("letra_busqueda") != null)
						init_user = Integer.parseInt(request.getParameter("init_user"));
					/*Procedente del servlet*/
					if(request.getAttribute("letra_busqueda_A") != null)
						init_user = (int) request.getAttribute("init_user_A");
					
					/*Se piden los usuarios que se se van a administrar*/
					List<UsuarioMin> allUser = userManager.obtenerUsersMin_word_number(init_user,30,request.getParameter("letra_busqueda"));
					
					/*Se actualiza el atributo de la request*/
					init_user =  init_user + 30;
					request.setAttribute("init_user_A",init_user);
					
					/*Se envia en la request la letra por la que se esta buscando*/
					request.setAttribute("letra_busqueda_A",request.getParameter("letra_busqueda"));
							
					/*Se devuelven en un atributo de la request*/
					request.setAttribute("allUser", allUser);
				}
				
				/*Se pone el valor de error a -1 para indicar que no hay errores*/
				request.setAttribute("error", -1);
				
				/*Se envia al administrador a la pagina de administracion*/
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Gestión de productos*/
		case 4:	
			
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			
			/*Si hay sesion*/
			else{

				/*Se indica en la sesión en que ventana de administración se encuentra el administrador*/
				request.getSession(false).setAttribute("Ventana","2");
				
				/*Se pone el valor de error a -1 para indicar que no hay errores*/
				request.setAttribute("error", -1);
				
				/*Se envia al administrador a la pagina de administracion*/
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Chats*/
		case 5:	
			
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
				
			}
			
			/*Si hay sesion*/
			else{
				
				/*variable auxiliar que indica si el selector escogido existe o no*/
				boolean selector_existente = true;
				
				/*Se crea un string auxiliar donde se recibiran los nuevos mensajes*/
				ArrayList<String> new_mensajes = null; 

				/*Se indica en la sesión en que ventana de administración se encuentra el administrador*/
				request.getSession(false).setAttribute("Ventana","3");
				
				/*Se indica que no ha sucedido ningun error*/
				request.setAttribute("error", -1);
				
				if(request.getParameter("accion_chat")!=null){
					
					/*Se crea el objeto para interactuar con la cola*/
					JMSManager mq = new JMSManager();
					
					/*Si al acción es 2 significa una escritura*/
					if(request.getParameter("accion_chat").equals("2")){
						
						/*Se comprueba si el usuario escogido existe*/
						if(userManager.findUsuarioMinByEmail(request.getParameter("destinatario")) == null){
							/*Se pone el valor de error a 12 para indicar que el usuario escogido no existe*/
								request.setAttribute("error", 12);
						}
						/*Si el usuario existe se escribe el mensaje en la cola*/
						else{
							/*Se llama a la función correspondiente para escribir en la cola de mensajes*/
							mq.escrituraJMS(request.getParameter("new_mensaje"), (String) request.getSession(false).getAttribute("e_mail"), request.getParameter("destinatario"));
						}
					}
					
					/*Si la accion es 1 significa la lectura de mensajes*/
					else if(request.getParameter("accion_chat").equals("1")){
						
						/*Se obtienen los mensajes de la cola de mensajes correspondientes al usuario*/
						new_mensajes = mq.lecturaJMS((String) request.getSession(false).getAttribute("e_mail"));
		
						/*Se introducen los mensajes en la request*/
						request.setAttribute("mensajes", new_mensajes);
						
						/*Se pone el error a 13 dbeido a que indica que no se disponen de mensajes nuevos*/
						if(new_mensajes == null || new_mensajes.isEmpty())
							request.setAttribute("error", 13);
					}
				}
	
				/*Se envia al administrador a la pagina de administracion*/
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
				
			}
			break;
			
		/*Cierre de sesión */	
		case 6:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			/*Si hay sesion*/
			else{
				
				/*Se indica el cierre de sesion por la terminal*/
				System.out.println("Cierre de session del usuario con e_mail: " + request.getSession(false).getAttribute("e_mail"));
				
				/*Se invalida la sesion*/
				request.getSession(false).invalidate();
				
				/*Se pone el valor de error a -1 para indicar que no hay errores*/
				request.setAttribute("error", -1);
				
				/*Se devuelve al administrador a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Modificación datos propios Administrdor*/	
		case 7:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			/*Si hay sesion*/
			else{
				
				/*Variable que informa del exito o no de la actualizacion de los datos del administrador, 1 exito, 2 fallo de pass, 3 fallo de base de datos*/
				int actualizacion_admin;
				
				/*Se realiza la actualizacion llamando a la funcion correspondiente*/
				actualizacion_admin = userManager.ModificarUser((String) request.getSession().getAttribute("e_mail"), request.getParameter("pass1"), request.getParameter("nombre"), request.getParameter("apellido1"), request.getParameter("apellido2"), request.getParameter("cod_postal"), request.getParameter("provincia"),request.getParameter("pass_old"));
				
				/*Si se actualiza correctamente*/
				if(actualizacion_admin == 1){
					
					/*Se indica por consola la correcta actualización*/
					System.out.println("Se ha modificado el administrador con e_mail: " +(String) request.getSession().getAttribute("e_mail"));
					
					/*Se pone el valor de error a -1 para indicar que no hay errores*/
					request.setAttribute("error", -1);
					
					/*Se hace la consulta de los propios datos del administrador*/
					UsuarioMin usuario = userManager.findUsuarioMinByEmail((String) request.getSession().getAttribute("e_mail"));

					/*Se mandan los datos en la request*/
					request.setAttribute("user_data", usuario);
					
					/*Se devuelve al usuario a la pagina de inicio*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
					
				}
				
				/*Si sucede un error en la actualización*/
				else{
					/*Se indica por terminal el error de modificacion*/
					System.out.println("Ha sucedido un error en la actualizacion del administrador con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
					
					/*El error 2 y 3 indica en administraccion que ha habido un fallo de actualizacion de info de administrador*/
					request.setAttribute("error", actualizacion_admin);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
			}
			break;
			
		/*Dar de baja al propio administrador*/	
		case 8:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			
			/*Si hay sesion*/
			else{
				/*int que indica si se ha realizado correctamente la baja del administrador, 1 exito,2 fallo de pass, 3 fallo base de datos */
				int baja_admin;
				
				/*Se realiza la baja llamando a la funcion correspondiente*/
				baja_admin = userManager.BajaUser((String) request.getSession().getAttribute("e_mail"),request.getParameter("contrasena"));
				
				/*Si se realiza la baja de forma correcta*/
				if(baja_admin == 1){
					
					/*Se indica por terminal que se ha realizado correctamente la baja del admin correspondiente*/
					System.out.println("Se ha borrado del administrador con e_mail: " +(String) request.getSession().getAttribute("e_mail"));
					
					/*Se pone el valor de error a -1 para indicar que no hay errores*/
					request.setAttribute("error", -1);
					
					/*Se invalida la sesion actual*/
					request.getSession(false).invalidate();
					
					/*Se devuelve al usuario al index*/
					reqDis = request.getRequestDispatcher("index.jsp");
					reqDis.forward(request, response);
				}
				
				//Si sucede un error se lleva a la pagina de error
				else{
					/*Se indica por terminal el error de baja*/
					System.out.println("Ha sucedido un error en la baja del administrador con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
					
					/*El error 4 y 5 indica en administraccion que ha habido un fallo de baja de administrador*/
					/*Fallo de contraseña*/
					if(baja_admin ==2)
						request.setAttribute("error", 4);
					/*Fallo de base de datos*/
					else
						request.setAttribute("error", 5);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
			}
			break;
			
			
		/*Creacion de un nuevo usuario o administrador*/
		case 9:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			else{
				/*int que indica si se ha realizado correctamente una inserccion de un administrador o user, 1 exito, 2 usuario existente, 3 fallo base datos*/
				int insercion_user = 3;
				
				/*Variable auxiliar que indica si el usuario tiene privilegios o no de administracion*/
				boolean administracion;
				
				/*Si el checkbox esta seleccionado se tienen permisos*/
				if(request.getParameter("admin")!=null)administracion = true;
				else administracion = false;
					
				/*Se llama a la función correspondiente el usuario*/
				insercion_user = userManager.insertarUserbyAdmin(request.getParameter("e_mailR"), request.getParameter("contrasenaR"), request.getParameter("nombre"), request.getParameter("apellido1"), request.getParameter("apellido2"), request.getParameter("cod_postal"), request.getParameter("provincia"),administracion);
			
				/*Si se inserta correctamente */
				if(insercion_user == 1){
					
					/*Se muestra por terminal la correcta inserccion*/
					System.out.println("Nuevo registro de usuario realizado con e_mail: " +request.getParameter("e_mailR"));
					
					/*Se pone el valor de error a -1 para indicar que no hay errores*/
					request.setAttribute("error", -1);
					
					/*Se realmacenan en la request los valores del punto de la administracion donde se encuentra el usuario*/
					if(request.getParameter("letra_busqueda") != null)
						request.setAttribute("letra_busqueda_A",request.getParameter("letra_busqueda"));
					if(request.getParameter("init_user")!= null)
						request.setAttribute("init_user_A",Integer.parseInt(request.getParameter("init_user"))-30);
					/*Se indica que se quiere ir a la gestion de usuarios*/
					request.setAttribute("n_peticion_A","3");
					
					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					reqDis = request.getRequestDispatcher("ControlServletAdmin");
					reqDis.forward(request, response);
				}
				
				/*Si sucede un error*/
				else{
					/*Se indica por terminal el error en la creacion del nuevo user*/
					System.out.println("Ha sucedido un error en la baja del administrador con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
					
					/*El error 6 y 7 indica en administraccion que ha habido un fallo insercción de usuario*/
					/*Fallo de contraseña*/
					if(insercion_user ==2)
						request.setAttribute("error", 6);
					/*Fallo de base de datos*/
					else
						request.setAttribute("error", 7);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
			}
			break;
			
		/*Modificacion de un usuario*/
		case 10:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			/*Si sucede hay sesion*/
			else{
			
				/*boolean que indica si se ha realizado correctamente una modificacion de un usuario*/
				boolean actualizacion_user;
				
				/*Variable auxiliar que indica si el usuario tiene privilegios o no de administración*/
				boolean administracion_user = false;
				//Si el checkbox esta seleccionado se tienen permisos.
				if(request.getParameter("adminM")!=null)administracion_user = true;
				else actualizacion_user = false;
				
				/*Se llama a la funcion correspondiente para la modificacion por parte de un administrador*/
				actualizacion_user = userManager.ModificarUserbyAdmin(request.getParameter("e_mailM"), request.getParameter("contrasenaM"), request.getParameter("nombreM"), request.getParameter("apellido1M"), request.getParameter("apellido2M"), request.getParameter("cod_postalM"), request.getParameter("provinciaM"),administracion_user);
				
				/*Si se modifica correctamente */
				if(actualizacion_user){
					
					/*Se muestra por terminal*/
					System.out.println("Modificacion de usuario realizado con e_mail: " +request.getParameter("e_mailM"));
					
					/*El error 1 siempre indica que se ha eliminado la sesion*/
					request.setAttribute("error", -1);
					
					/*Se realmacenan en la request los valores del punto de la administracion donde se encuentra el usuario*/
					if(request.getParameter("letra_busqueda") != null)
						request.setAttribute("letra_busqueda_A",request.getParameter("letra_busqueda"));
					if(request.getParameter("init_user")!= null)
						request.setAttribute("init_user_A",Integer.parseInt(request.getParameter("init_user"))-30);
					/*Se indica que se quiere ir a la gestion de usuarios*/
					request.setAttribute("n_peticion_A","3");
					
					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					reqDis = request.getRequestDispatcher("ControlServletAdmin");
					reqDis.forward(request, response);
				}
				
				/*Si sucede un error */
				else{
					
					/*Se inidica por terminal el fallo de la modificacion*/
					System.out.println("No se ha realizado el nuevo registro");
					
					/*Error 9 inidica error en la modificacion de un usuario*/
					request.setAttribute("error", 8);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
			}
			break;
			
		/*Dar de baja a un usuario*/
		case 11:
			/*Se comprueba que hay sesion*/
			if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
				
				/*Se inidica por consola el error*/
				System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
				
				/*El error 1 siempre indica que se ha eliminado la sesion*/
				request.setAttribute("error", 1);
				
				/*Se devuelve al usuario a la pagina de inicio*/
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
				
			}
			/*Si hay sesion*/
			else{
				/*Booleano que indica si se ha realizado correctamente una baja*/
				boolean baja_user;
				
				/*Se llama a la función correspondiente para la baja de usuarios por un administrador*/
				baja_user = userManager.BajaUserbyAdmin((String) request.getParameter("e_mailB"));
				
				/*Si se realiza la baja*/
				if(baja_user){
					
					/*Se indica por terminal la correctamente la baja*/
					System.out.println("Se ha borrado el usuario con e_mail: " +(String) request.getParameter("e_mailB"));
					
					/*Se pone el valor de error a -1 para indicar que no hay errores*/
					request.setAttribute("error", -1);
					
					/*Se realmacenan en la request los valores del punto de la administracion donde se encuentra el usuario*/
					if(request.getParameter("letra_busqueda") != null)
						request.setAttribute("letra_busqueda_A",request.getParameter("letra_busqueda"));
					if(request.getParameter("init_user")!= null)
						request.setAttribute("init_user_A",Integer.parseInt(request.getParameter("init_user"))-30);
					/*Se indica que se quiere ir a la gestion de usuarios*/
					request.setAttribute("n_peticion_A","3");

					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					reqDis = request.getRequestDispatcher("ControlServletAdmin");
					reqDis.forward(request, response);
				}
				
				//Si sucede un error se lleva a la pagina de error
				else{
					
					/*Se inidica por terminal el fallo de la baja*/
					System.out.println("Ha sucedido un error en borrado del usuario con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
					
					/*Error 9 inidica error en la baja de un usuario*/
					request.setAttribute("error", 9);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
			}
			break;
			
			/*Busqueda simple de productos*/
			case 12:
				/*Se comprueba que hay sesion*/
				if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
					
					/*Se inidica por consola el error*/
					System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
					
					/*El error 1 siempre indica que se ha eliminado la sesion*/
					request.setAttribute("error", 1);
					
					/*Se devuelve al usuario a la pagina de inicio*/
					reqDis = request.getRequestDispatcher("index.jsp");
					reqDis.forward(request, response);
					
				}
				/*Si hay sesion*/
				else{
					
					/*Lista de productos obtenidos en la busqueda simple*/
					List<Producto> simple_search = null;
					
					/*Si la categoria es distinta de la de por defecto*/
					if(!request.getParameter("category").equals("Todo")){
						
						/*Si la busqueda no es vacia, se busca en dicha categoria las coincidencias de la busqueda en titulo y descripción*/
						if(!request.getParameter("search").isEmpty()){
							simple_search = productManager.obtenerAllProducts_wordsAndCategory(request.getParameter("search"),request.getParameter("category"));
						}
						/*Si la busqueda es vacia, se dan todos los productos de la correspondiente categoria*/
						else{
							simple_search = productManager.findProductoByCategoria(request.getParameter("category"));
						}

					}
					
					/*Si la categoria es por defecto*/
					else{
						/*Si la busqueda no es vacia, se busca en todas las categorias las coincidencias de la busqueda en titulo y descripcion*/
						if(!request.getParameter("search").isEmpty()){
							simple_search = productManager.obtenerAllProducts_words(request.getParameter("search"));
						}
						/*Si tanto la busqueda como la categoria son vacias se devuelven todos los productos*/
						else{
							simple_search = productManager.obtenerAllProducts();
						}
						
					}
					
					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					if(!simple_search.isEmpty()){
						
						/*Se obtienen las imagenes de los productos*/
						InputStream in;
						BufferedImage fichero_imagen;
						List <String> img_name = new ArrayList <String> ();
						List <Imagene> img;
						List <String> rutas = new ArrayList <String> ();
						
						int contador = 0;
						List <Integer> numrutas = new ArrayList <Integer> ();
						
						for (int i = 0; i < simple_search.size(); i++){
							
							/*Para cada producto, cogemos su imagen principal*/	
							img = imagenmanager.obtenerImagenesByProducto(simple_search.get(i).getId());
							
							for(int k = 0 ; k<img.size() ; k++){
								img_name.add(Integer.toString(img.get(k).getId().getIdProd()) + img.get(k).getId().getNumImg());
							}
			
							for(int j = 0; j < img.size(); j++){
								
								in = new ByteArrayInputStream(img.get(j).getImagen());
							
								fichero_imagen = ImageIO.read(in);
							
								File img_save = new File (path + img_name.get(j) + ".jpg");
							
								ImageIO.write(fichero_imagen, "jpg", img_save);
							
								rutas.add("temp/" + img_name.get(j) + ".jpg");
								
								contador ++;

							}
							
							//productos.add(new resultProducts(simple_search.get(i),));
							numrutas.add(contador);
							
							contador = 0;
							img_name = new ArrayList <String> ();
						}
						

						/*Se indica por terminal la correctamente la baja*/
						System.out.println("Se han encontrado productos para la busqueda: "+ request.getParameter("search") + ", con categoría:" + request.getParameter("category"));
						
						/*Se pone el valor de error a -1 para indicar que no hay errores*/
						request.setAttribute("error", -1);
						
						/*Se devuelven en un atributo de la request los productos*/
						request.setAttribute("allProducts", simple_search);
						
						/*Se devuelven en un atributo de la request las rutas a sus imagenes*/
						request.setAttribute("rutas", rutas);
						
						/*Se devuelven en un atributo de la request el numero de rutas para cada producto*/
						request.setAttribute("numrutas", numrutas);

						/*Se devuelve a la administración*/
						reqDis = request.getRequestDispatcher("administracion.jsp");
						reqDis.forward(request, response);
					}
					
					/*Si no se encuentran resultados*/
					if(simple_search.isEmpty()){
		
						System.out.println("No se han encontrado productos para la busqueda: "+ request.getParameter("search") + ", con categoría:" + request.getParameter("category"));
						
						/*Error 10 inidica que no se han encontrado productos para la busqueda*/
						request.setAttribute("error", 10);
						
						/*Se devuelve a administracion*/
						reqDis = request.getRequestDispatcher("administracion.jsp");
						reqDis.forward(request, response);
					}
				}
				break;
			/*Busqueda avanzada*/
			case 13:
				/*Se comprueba que hay sesion*/
				if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
					
					/*Se inidica por consola el error*/
					System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
					
					/*El error 1 siempre indica que se ha eliminado la sesion*/
					request.setAttribute("error", 1);
					
					/*Se devuelve al usuario a la pagina de inicio*/
					reqDis = request.getRequestDispatcher("index.jsp");
					reqDis.forward(request, response);
					
				}
				/*Si hay sesion*/
				else{
					
					/*Lista de productos obtenidos en la busqueda simple*/
					List<Producto> advanced_search = null;
					
					advanced_search = productManager.advSearch(request.getParameter("categoryAdv"),request.getParameter("provincia"),request.getParameter("vendedor"),request.getParameter("title"),request.getParameter("descrip"));
					
					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					if(!advanced_search.isEmpty()){
						/*Se obtienen las imagenes de los productos*/
						InputStream in;
						BufferedImage fichero_imagen;
						List <String> img_name = new ArrayList <String> ();
						List <Imagene> img;
						List <String> rutas = new ArrayList <String> ();
						
						int contador = 0;
						List <Integer> numrutas = new ArrayList <Integer> ();
						
						for (int i = 0; i < advanced_search.size(); i++){
							
							/*Para cada producto, cogemos su imagen principal*/	
							img = imagenmanager.obtenerImagenesByProducto(advanced_search.get(i).getId());
							
							for(int k = 0 ; k<img.size() ; k++){
								img_name.add(Integer.toString(img.get(k).getId().getIdProd()) + img.get(k).getId().getNumImg());
							}
			
							for(int j = 0; j < img.size(); j++){
								
								in = new ByteArrayInputStream(img.get(j).getImagen());
							
								fichero_imagen = ImageIO.read(in);
							
								File img_save = new File (path + img_name.get(j) + ".jpg");
							
								ImageIO.write(fichero_imagen, "jpg", img_save);
							
								rutas.add("temp/" + img_name.get(j) + ".jpg");
								
								contador ++;

							}
							
							//productos.add(new resultProducts(simple_search.get(i),));
							numrutas.add(contador);
							
							contador = 0;
							img_name = new ArrayList <String> ();
						}

						/*Se indica por terminal la correctamente la baja*/
						System.out.println("Se han encontrado productos para la busqueda: Categoria:"+ request.getParameter("categoryAdv") + ", Provincia:" + request.getParameter("provincia") + ", Titulo:" + request.getParameter("title")+ ", Provincia:" + request.getParameter("provincia") +", Descripcion:"+request.getParameter("descrip"));
						
						/*Se pone el valor de error a -1 para indicar que no hay errores*/
						request.setAttribute("error", -1);
						
						/*Se devuelven en un atributo de la request*/
						request.setAttribute("allProducts", advanced_search);
						
						/*Se devuelven en un atributo de la request las rutas a sus imagenes*/
						request.setAttribute("rutas", rutas);
						
						/*Se devuelven en un atributo de la request el numero de rutas para cada producto*/
						request.setAttribute("numrutas", numrutas);

						/*Se devuelve a la administración*/
						reqDis = request.getRequestDispatcher("administracion.jsp");
						reqDis.forward(request, response);
					}
					
					/*Si no se encuentran resultados*/
					if(advanced_search.isEmpty()){
		
						System.out.println("No se han encontrado productos para la busqueda: Categoria:"+ request.getParameter("categoryAdv") + ", Provincia:" + request.getParameter("provincia") + ", Vendedor:" + request.getParameter("vendedor")+ ", Descripcion:"+request.getParameter("descrip"));
						
						/*Error 10 inidica que no se han encontrado productos para la busqueda*/
						request.setAttribute("error", 11);
						
						/*Se devuelve a administracion*/
						reqDis = request.getRequestDispatcher("administracion.jsp");
						reqDis.forward(request, response);
					}
				}
				break;
				
			/*Modificar un producto*/
			case 14:
				// Se comprueba si hayu una sesion activa
				if(request.getSession(false) == null){
					System.out.println("Se ha intentado cerrar una sesion sin sesion o sesion caducada");
					HttpSession session = request.getSession(true);
					session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
					reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
					reqDis.forward(request, response);
				}
				
				List <Imagene> imagenesU = null;
				
				/*Obtenemos el propietario del producto*/
				Usuario usuario1 = userManager.findUsuarioByEmail(request.getParameter("e_mail_user"));
				
				if(usuario1 == null)
					System.out.println("No se ha obtenido usuario");
				
				/*Actualizamos los datos del producto. */
				if(!productManager.modificarProducto(Integer.parseInt(request.getParameter("hidden_mod")), request.getParameter("category_pid"), Integer.parseInt(request.getParameter("estado_id")), 0, Float.parseFloat(request.getParameter("precioPID")), request.getParameter("nombrePID"), request.getParameter("comentario1"), imagenesU, usuario1))
					System.out.println("No ha ido bien la actualizacion");
				
				
				
				Producto productoActualizado = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));		
				
				/*Modificamos las imagenes si el usuario ha introducido alguna en el formulario*/
				
				String imagenRequestU = "";
				InputStream inputStreamU;
				Part filePartU;
				ImagenePK idPKU = new ImagenePK ();
				
				
				for (int i = 0; i < 4; i ++){
					
					inputStreamU = null;
					filePartU = null;
					
					switch (i){
					
					case (0):
						
						imagenRequestU = "imagenP1O";
						idPKU.setNumImg(0);
						
						break;
						
					case (1):
						
						imagenRequestU = "imagenP2O";
						idPKU.setNumImg(1);
						break;
						
					case (2):
						
						imagenRequestU = "imagenP3O";
						idPKU.setNumImg(2);
						break;
						
					case (3):
					
						imagenRequestU = "imagenP4O";
						idPKU.setNumImg(3);
						break;
					
					default:			
				
					}
					
					filePartU = request.getPart(imagenRequestU);
					

			            
					if (filePartU != null && filePartU.getSize()> 0 && !filePartU.getContentType().equals("application/octet-stream")) {
						System.out.println("He detectado una insercci�n de imagen");
						
						inputStreamU = filePartU.getInputStream();
						
						idPKU.setIdProd(productoActualizado.getId());
						
						byte[] dataU = new byte[(int) filePartU.getSize()];
						int nReadU;
						ByteArrayOutputStream bufferU = new ByteArrayOutputStream();
						while ((nReadU = inputStreamU.read(dataU, 0, dataU.length)) != -1) {
						  bufferU.write(dataU, 0, nReadU);
						}
			
						bufferU.flush();
						
						/*Necesito hacer la actualizacion. Para hacer la actualizacion, necesito poder buscar la imagen que quiero PK*/
						//imagenmanager.insertarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
						
						if (i < productoActualizado.getNumImg()){
							imagenmanager.modificarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
						}
						
						else {
							imagenmanager.insertarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
						}
					}
					
				}
				
				
				request.setAttribute("precioPerfil", productoActualizado.getPrecio());
				request.setAttribute("nombrePerfil", productoActualizado.getTitulo());
				request.setAttribute("categoriaPerfil", productoActualizado.getCategoria());
				request.setAttribute("descripcionPerfil", productoActualizado.getDescripcion());
				request.setAttribute("estadoPerfil", productoActualizado.getEstado());
				
				request.setAttribute("usuarioPerfil", request.getParameter("usuarioPerfil"));
				request.setAttribute("id", request.getParameter("id_oculto"));
				request.setAttribute("ciudad", request.getParameter("provincia_oculta"));
				request.setAttribute("codPostal", request.getParameter("cod_postal_oculta"));
				
				
				Imagene imgPR;
				String img_nameR = "";
				InputStream inRU;
				BufferedImage fichero_imagenRU;
				List <String> rutasR = new ArrayList <String> ();	
				/*Nos recorremos las imagenes del producto del que venimos y las mostramos*/
				
				/*Obtenemos de nuevo el producto, con el numImg modificado (por trigger)*/
				productoActualizado = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));
				
				for (int i = 0; i < productoActualizado.getNumImg(); i++){
					
					imgPR = imagenmanager.obtenerImagenesByProducto(Integer.parseInt(request.getParameter("id_oculto"))).get(i);
					
					//int hola = imgPR.getId().getNumImg();
					//System.out.println("El numImg obtenido es: " + hola);
					
					/*Establecemos el nombre del archivo en funcion de el ID del rpdocuto y el numero de imagen*/
					img_nameR = Integer.toString(imgPR.getId().getIdProd()) + imgPR.getId().getNumImg();
					
					inRU= new ByteArrayInputStream(imgPR.getImagen());
					
					fichero_imagenRU = ImageIO.read(inRU);
					
					File img_saveR = new File (path + img_nameR + ".jpg");
					
					ImageIO.write(fichero_imagenRU, "jpg", img_saveR);
					
					rutasR.add("temp/" + img_nameR + ".jpg");
					System.out.println("Desde product.jsp hemos insertado en esta ruta: " + rutasR.get(i));
					
				}
				
				request.setAttribute("rutasR", rutasR);
				request.setAttribute("numImgR", productoActualizado.getNumImg());
				
				reqDis = request.getRequestDispatcher("administracion.jsp");
				reqDis.forward(request, response);
				
				break;
				
				
			/*Dar de baja un producto*/
			case 15:
				/*Booleano que indica si se ha realizado correctamente una baja de un producto*/
				boolean baja_producto;
				
				/*Se llama a la función correspondiente para la baja de usuarios por un administrador*/
				baja_producto = productManager.BajaProductbyAdmin((String) request.getParameter("id_b"));
				
				/*Si se realiza la baja*/
				if(baja_producto){
					
					/*Se indica por terminal la correctamente la baja*/
					System.out.println("Se ha borrado el producto con ID: " + request.getParameter("id_b"));
					
					/*Se pone el valor de error a -1 para indicar que no hay errores*/
					request.setAttribute("error", -1);

					/*Se devuelve reenvia la peticion a el servlet para que le lleve al punto de administración correspondiente*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
				
				//Si sucede un error se lleva a la pagina de error
				else{
					
					/*Se inidica por terminal el fallo de la baja*/
					System.out.println("Ha sucedido un error en borrado del producto con ID:" +request.getParameter("id_b"));
					
					/*Error 9 inidica error en la baja de un usuario*/
					request.setAttribute("error", 11);
					
					/*Se devuelve a administracion*/
					reqDis = request.getRequestDispatcher("administracion.jsp");
					reqDis.forward(request, response);
				}
				break;
				
		default:
			System.out.println("no se puede llegar a este punto");
			reqDis = request.getRequestDispatcher("sorryNotFound.jsp");
		}

	}

}
