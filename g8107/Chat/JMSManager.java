
package g8107.Chat;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

public class JMSManager {

	private javax.jms.ConnectionFactory factory = null;
	private javax.naming.InitialContext contextoInicial = null;
	private javax.jms.Destination cola = null;
	private javax.jms.Connection Qcon = null;
	private javax.jms.Session QSes = null;
	private javax.jms.MessageProducer Mpro = null;
	private javax.jms.MessageConsumer Mcon = null;

	public void escrituraJMS(String mensaje,String usrorigen, String selector) {

		try {

			contextoInicial = new javax.naming.InitialContext();

			/*Obtenemos el conection factory*/
				factory = (javax.jms.ConnectionFactory) contextoInicial
						.lookup(InformacionProperties.getQCF());
			/*Obtenemos la cola donde se almacenan los mensajes*/
				cola = (javax.jms.Destination) contextoInicial
						.lookup(InformacionProperties.getQueue());
			/*Creamos la comunicación y la sesion sin transacciones*/
			Qcon = factory.createConnection();
			QSes = Qcon
					.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);

			Mpro = QSes.createProducer(cola);

			javax.jms.TextMessage men = QSes.createTextMessage();
			/*El mensaje estara compuesto por el usuario que lo envia y el mensajes*/
			mensaje=usrorigen+"/"+mensaje;
			/*Insertamos el mensaje en un determinado selector, este selector sera el destino*/
			men.setText(mensaje);
			men.setJMSCorrelationID(selector);
			Qcon.start();
			/*Enviamos el mensaje*/
			Mpro.send(men);

			this.Mpro.close();
			this.QSes.close();
			this.Qcon.close();

		} catch (javax.jms.JMSException e) {
			System.out
					.println("Error en la configuracion de JMS: "
							+ e.getLinkedException().getMessage());
			System.out
					.println("Error en la configuracion de JMS: "
							+ e.getLinkedException().toString());
		} catch (Exception e) {
			System.out
					.println("Error Exception: "
							+ e.getMessage());
		}

	}

	public ArrayList<String> lecturaJMS(String strSelectorPasado) {

		
		try {
			contextoInicial = new javax.naming.InitialContext();

			/*Obtenemos el Connection factory*/
				factory = (javax.jms.ConnectionFactory) contextoInicial
						.lookup(InformacionProperties.getQCF());
			/*Obtenemos la cola donde se almacena la conexion*/
				cola = (javax.jms.Destination) contextoInicial
						.lookup(InformacionProperties.getQueue());
			/*Creamos la comunicación y la sesion sin transacciones*/
			Qcon = factory.createConnection();

			QSes = Qcon
					.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			
			/*Buscamos por el selector que recibimos, este sera el nombre del usuario que quiere leer sus mensajes*/
			String sSelector = "JMSCorrelationID = '"
					+ strSelectorPasado.trim() + "'";
			
			/*Busca los mensajes dependiendo del selector*/
			if (strSelectorPasado.equals("")) {
				Mcon = QSes.createConsumer(cola);
			} else {
				Mcon = QSes.createConsumer(cola, sSelector);

			}
			
			/*Creamos unos ArrayList para los metodos posteriores*/
			ArrayList<String> Usuarios = new ArrayList<String>();
			ArrayList<String> Mensajes = new ArrayList<String>();
			ArrayList<String> Mensajes_ordenados = new ArrayList<String>();
			
			Qcon.start();
			Message mensaje = null;
			
			/*Separamos los usuarios de los mensajes, para ello introducimos cada cual en un ArrayList distinto*/
			while (true) {
				mensaje = Mcon.receive(100);
				if (mensaje != null) {
					if (mensaje instanceof TextMessage) {
						TextMessage m = (TextMessage) mensaje;
						String [] separar = m.getText().split("/");
						Usuarios.add(separar[0]);
						Mensajes.add(separar[1]);
					} else {
						break;
					}
				} else // NO existe mensaje, mensaje es null
				{
					
					break;
				}
			
			}
			/*Una vez tenemos los usuarios y los mensajes en ArrayList separados,
			 * los ordenamos de nuevo en un ArrayList donde hay una posicion por cada
			 * usuario y se concatenen los mensajes en esa posicion
			 */
			int num_mensaje = 0;
			for (int i=0; i<Usuarios.size();i++){
				if (Usuarios.get(i) == null);
				else{
				Mensajes_ordenados.add(Usuarios.get(i)+":<br>" + "<div id=\"text-chat\">" + Mensajes.get(i) + "</div>");
				
				for (int e=i+1; e<Usuarios.size();e++){
					if (Usuarios.get(i).equals(Usuarios.get(e))){
						Mensajes_ordenados.set(num_mensaje, Mensajes_ordenados.get(num_mensaje) + "<div id=\"text-chat\">" + Mensajes.get(e) + " </div>");
						Usuarios.set(e, null);
						}
					}
				Mensajes_ordenados.set(num_mensaje, Mensajes_ordenados.get(num_mensaje) + " <br> No hay mas Mensajes del usuario: "+ Usuarios.get(i));
				
				num_mensaje++;
				}
			}
			
			this.Mcon.close();
			this.QSes.close();
			this.Qcon.close();
			
			return Mensajes_ordenados;

		} catch (javax.jms.JMSException e) {
			System.out
					.println("Error en la configuracion de JMS: "
							+ e.getLinkedException().getMessage());
			System.out
					.println("Error en la configuracion de JMS: "
							+ e.getLinkedException().toString());
		} catch (Exception e) {
			System.out
					.println("Error Exception: "
							+ e.getMessage());
			e.printStackTrace();
		}

		return null;

	}
		
}

